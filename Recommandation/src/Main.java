import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    private static final boolean IGNORE_BAD_SIMILITUDE_NEIGHBORS = true;

    private static final String dataFilePath = "../MovieLens/100K/ml-100k/u.data";
    private static final String baseFilePath = "../MovieLens/100K/ml-100k/u%s.base";
    private static final String testFilePath = "../MovieLens/100K/ml-100k/u%s.test";
    private static final String userFilePath = "../MovieLens/100K/ml-100k/u.user";
    private static final String itemFilePath = "../MovieLens/100K/ml-100k/u.item";
    private static final int K = 10;
    private static final int TEST_FILES = 5;

    private static List<Rating> ratings = new ArrayList<>();
    private static List<Rating> tests = new ArrayList<>();
    private static List<User> users = new ArrayList<>();
    private static List<Item> items = new ArrayList<>();
    private static double[][] userSimilarities;
    private static int[][] itemSimilarities;

    public static void main(String[] args) {
        try {
            readUsers();
            readItems();
            //readData(dataFilePath);
            double meanRMSE = 0, meanMAE = 0;
            for (int i = 1; i <= TEST_FILES; ++i) {
                ratings.clear();
                tests.clear();
                readBase(i);
                readTest(i);
                calcUserSimilarities();
                calcItemSimilarities();

                double rmse = 0, mae = 0;
                for (Rating test : tests) {
                    double predictedRating = predict(test.getUserId(), test.getItemId());
                    double diff = Math.abs(predictedRating - test.getRating());
                    mae += diff;
                    rmse += diff * diff;
                }
                mae /= tests.size();
                rmse = Math.sqrt(rmse / tests.size());
                System.out.println(String.format("%d: MAE %.03f RMSE %.03f", i, mae, rmse));
                meanRMSE += rmse;
                meanMAE += mae;
            }
            meanMAE /= TEST_FILES;
            meanRMSE /= TEST_FILES;
            System.out.println(String.format("Mean MAE %.03f RMSE %.03f", meanMAE, meanRMSE));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static double predict(int userId, int itemId) {
        User user = users.get(userId - 1);
        List<User> unn = KNN(user, K, itemId);
        if(unn.size() > 0) {
            double count = 0;
            double sum = 0;
            for (User neigh : unn) {
                Rating r = neigh.getRating(itemId);
                double sim = userSimilarities[userId - 1][neigh.getUserId() - 1];
                sum += sim * (r.getRating() - neigh.getAvgRating());
                count += sim;
            }
            return user.getAvgRating() + sum / count;
        }
        return user.getAvgRating();
    }

    private static void readUsers() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(userFilePath));
        String line;
        String[] lineValues;
        while ((line = reader.readLine()) != null) {
            lineValues = line.split("\\|");
            users.add(new User(Integer.valueOf(lineValues[0]), Integer.valueOf(lineValues[1]), lineValues[2].charAt(0), lineValues[3], lineValues[4]));
        }
    }

    private static void readItems() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(itemFilePath));
        String line;
        String[] lineValues;
        while ((line = reader.readLine()) != null) {
            lineValues = line.split("\\|");
            Item item = new Item(
                    Integer.valueOf(lineValues[0]),
                    lineValues[1],
                    lineValues[2],
                    lineValues[3],
                    lineValues[4],
                    new int[19]);
            for (int i = 0; i < 19; ++i)
                item.getTypes()[i] = Integer.valueOf(lineValues[i + 5]);
            items.add(item);
        }
    }

    private static void readData(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line;
        String[] lineValues;
        int userId, itemId;
        Rating rating;
        while ((line = reader.readLine()) != null) {
            lineValues = line.split("\\t");
            userId = Integer.valueOf(lineValues[0]);
            itemId = Integer.valueOf(lineValues[1]);
            rating = new Rating(userId, itemId, Integer.valueOf(lineValues[2]), Long.valueOf(lineValues[3]));
            ratings.add(rating);
            users.get(userId - 1).getRatings().put(itemId, rating);
            items.get(itemId - 1).getRatings().put(userId, rating);
        }
    }

    private static void readBase(int i) throws IOException {
        readData(String.format(baseFilePath, i));
    }

    private static void readTest(int i) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(String.format(testFilePath, i)));
        String line;
        String[] lineValues;
        int userId, itemId;
        Rating rating;
        while ((line = reader.readLine()) != null) {
            lineValues = line.split("\\t");
            userId = Integer.valueOf(lineValues[0]);
            itemId = Integer.valueOf(lineValues[1]);
            rating = new Rating(userId, itemId, Integer.valueOf(lineValues[2]), Long.valueOf(lineValues[3]));
            tests.add(rating);
        }
    }

    private static void calcUserSimilarities() {
        userSimilarities = new double[users.size()][users.size()];
        for (int i = 0; i < users.size(); ++i) {
            for (int j = i + 1; j < users.size(); ++j) {
                userSimilarities[i][j] = userSimilarities[j][i] = users.get(i).similarity(users.get(j));
            }
        }
    }

    private static void calcItemSimilarities() {
        itemSimilarities = new int[items.size()][items.size()];
        for (int i = 0; i < items.size(); ++i) {
            for (int j = 0; j < items.size(); ++j) {
                itemSimilarities[i][j] = itemSimilarities[j][i] = items.get(i).similarity(items.get(j));
            }
        }
    }

    private static List<User> KNN(User user, int k, int hasRatedItemId) {
        Map<User, Double> neighbors = new HashMap<>();
        for (int i = 0; i < userSimilarities.length; ++i) {
            if (user.getUserId() != i + 1 && users.get(i).getRating(hasRatedItemId) != null && (!IGNORE_BAD_SIMILITUDE_NEIGHBORS || userSimilarities[i][user.getUserId() - 1] > 0))
                neighbors.put(users.get(i), userSimilarities[i][user.getUserId() - 1]);
        }
        return neighbors.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList())
                .subList(0, Math.min(k, neighbors.size()));
    }

    private static List<Item> KNN(Item item, int k) {
        //TODO consider ratings also
        Map<Item, Integer> neighbors = new HashMap<>();
        for (int i = 0; i < itemSimilarities.length; ++i) {
            if (item.getMovieId() != i + 1)
                neighbors.put(items.get(i), itemSimilarities[i][item.getMovieId() - 1]);
        }
        return neighbors.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList())
                .subList(0, k);
    }
}
