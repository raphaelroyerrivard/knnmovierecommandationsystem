public class Rating {
    public static final int MIN = 1;
    public static final int MAX = 5;

    private int userId;
    private int itemId;
    private int rating;
    private long timestamp;

    public Rating(int userId, int itemId, int rating, long timestamp) {
        this.userId = userId;
        this.itemId = itemId;
        this.rating = rating;
        this.timestamp = timestamp;
    }

    public int getUserId() {
        return userId;
    }

    public int getItemId() {
        return itemId;
    }

    public int getRating() {
        return rating;
    }

    @Override
    public String toString() {
        return String.format("<Rating userId:%s itemId:%s rating:%s timestamp:%s>", userId, itemId, rating, timestamp);
    }
}
