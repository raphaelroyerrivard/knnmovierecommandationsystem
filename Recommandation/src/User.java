import java.util.*;
import java.util.stream.Collectors;

public class User {
    private int userId;
    private int age;
    private char gender;
    private String occupation;
    private String zipCode;
    private Map<Integer, Rating> ratings = new HashMap<>();

    public User(int userId, int age, char gender, String occupation, String zipCode) {
        this.userId = userId;
        this.age = age;
        this.gender = gender;
        this.occupation = occupation;
        this.zipCode = zipCode;
    }

    public int getUserId() {
        return userId;
    }

    public Map<Integer, Rating> getRatings() {
        return ratings;
    }

    public Rating getRating(int itemId) {
        return this.ratings.getOrDefault(itemId, null);
    }

    public static double similarity(User u1, User u2) {
        //PC similarity
        double meanR1 = 0, meanR2 = 0;

        for (Rating r : u1.ratings.values())
            meanR1 += r.getRating();
        meanR1 /= u1.ratings.size();

        for (Rating r : u2.ratings.values())
            meanR2 += r.getRating();
        meanR2 /= u2.ratings.size();

        double sum1 = 0, sum2 = 0, sum3 = 0;
        for (Rating r1 : u1.ratings.values()) {
            Rating r2 = u2.getRating(r1.getItemId());
            if (r2 != null) {
                double dr1 = r1.getRating() - meanR1;
                double dr2 = r2.getRating() - meanR2;
                sum1 += dr1 * dr2;
                sum2 += dr1 * dr1;
                sum3 += dr2 * dr2;
            }
        }
        if(sum1 * sum2 * sum3 == 0)
            return 0;
        return sum1 / Math.sqrt(sum2 * sum3);
    }

    public double similarity(User u) {
        return similarity(this, u);
    }

    public double getAvgRating() {
        int sum = 0;
        for(Rating rating : ratings.values())
            sum += rating.getRating();
        return (double) sum / ratings.size();
    }

    @Override
    public String toString() {
        StringBuilder ratingsString = new StringBuilder("[ ");

        List<Rating> ratings = this.ratings.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());

        for (Rating rating : ratings) {
            ratingsString.append(rating.getItemId()).append("-").append(rating.getRating()).append(", ");
        }
        ratingsString = new StringBuilder(ratingsString.substring(0, ratingsString.length() - 2) + " ]");
        return String.format("<User userId:%s, age:%s, gender:%s, occupation:%s, zipCode:%s, ratings:%s>", userId, age, gender, occupation, zipCode, ratingsString.toString());
    }
}
