import java.util.HashMap;
import java.util.Map;

public class Item {
    private int movieId;
    private String movieTitle;
    private String releaseDate;
    private String videoReleaseDate;
    private String IMDbURL;
    private int[] types;
    private Map<Integer, Rating> ratings = new HashMap<>();
    /* TYPES
    unknown|0
    Action|1
    Adventure|2
    Animation|3
    Children's|4
    Comedy|5
    Crime|6
    Documentary|7
    Drama|8
    Fantasy|9
    Film-Noir|10
    Horror|11
    Musical|12
    Mystery|13
    Romance|14
    Sci-Fi|15
    Thriller|16
    War|17
    Western|18
    */

    public Item(int movieId, String movieTitle, String releaseDate, String videoReleaseDate, String IMDbURL, int[] types) {
        this.movieId = movieId;
        this.movieTitle = movieTitle;
        this.releaseDate = releaseDate;
        this.videoReleaseDate = videoReleaseDate;
        this.IMDbURL = IMDbURL;
        this.types = types;
    }

    public int getMovieId() {
        return movieId;
    }

    public int[] getTypes() {
        return types;
    }

    public int similarity(Item item) {
        int similitude = 0;
        for(int i=0; i<types.length; ++i)
            if(types[i] == 1 && item.types[i] == 1)
                ++similitude;
        return similitude;
    }

    public Map<Integer, Rating> getRatings() {
        return ratings;
    }

    @Override
    public String toString() {
        return String.format("<Item movieId:%s, movieTitle:%s, releaseDate:%s, videoReleaseDate:%s, IMDbURL:%s>", movieId, movieTitle, releaseDate, videoReleaseDate, IMDbURL);
    }
}
